#ANDROID APPLICATION MAKEFILE
APP_BUILD_SCRIPT := $(call my-dir)/Android.mk
APP_PROJECT_PATH := $(call my-dir)

APP_OPTIM := release

APP_PLATFORM := android-16
APP_STL := gnustl_static
APP_CPPFLAGS := -fexceptions -frtti
#APP_CPPFLAGS := -Os -mthumb-interwork -fno-short-enums
#APP_CPPFLAGS := -Wl,--no-undefined

APP_ABI := armeabi-v7a

#APP_MODULES      :=  geos proj sqlite3
#APP_MODULES := jpeg gif tiff png ft2 zlib curl 
APP_MODULES := zlib

