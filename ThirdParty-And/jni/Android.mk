LOCAL_PATH := $(call my-dir)

CURRENT_PATH := $(call my-dir)
GEOS_PATH := geos
PROJ4_PATH := proj
SQLITE_PATH := sqlite3

include $(CURRENT_PATH)/$(PROJ4_PATH)/proj4.mk
include $(CURRENT_PATH)/$(GEOS_PATH)/geos.mk
include $(CURRENT_PATH)/$(SQLITE_PATH)/sqlite.mk
#include $(CURRENT_PATH)/libpng/Android.mk
#include $(CURRENT_PATH)/libtiff/Android.mk
#include $(CURRENT_PATH)/libjpeg/Android.mk
#include $(CURRENT_PATH)/giflib/Android.mk
#include $(CURRENT_PATH)/gdal/Android.mk
#include $(CURRENT_PATH)/freetype/Android.mk
#include $(CURRENT_PATH)/zlib/Android.mk
#include $(CURRENT_PATH)/curl/Android.mk
include $(CURRENT_PATH)/zlib/Android.mk
