if [ ! -d "build_and" ];then
	mkdir build_and
fi

cd build_and
echo $ANDROID_NDK
cmake ..\
 -DANDROID_NDK=$ANDROID_NDK\
 -DANDROID=TRUE\
 -DOPENGL_PROFILE=GLES3\
 -DDYNAMIC_OPENTHREADS=OFF\
 -DDYNAMIC_OPENSCENEGRAPH=OFF\
 -DANDROID_DEBUG=ON\
 -DANDROID_OPTIM_NEON=ON\
 -DOSG_GL_MATRICES_AVAILABLE=OFF\
 -DOSG_GL_VERTEX_FUNCS_AVAILABLE=OFF\
 -DOSG_GL_VERTEX_ARRAY_FUNCS_AVAILABLE=OFF\
 -DOSG_GL_FIXED_FUNCTION_AVAILABLE=OFF\
 -DANDROID_STL=gnustl_static\
 -DANDROID_PLATFORM=android-21\
 -DANDROID_ABI="armeabi-v7a"\
 -DJ=16 \
