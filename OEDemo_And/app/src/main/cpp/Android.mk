LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE    := osgNativeLib
### Main Install dir
#ANDROID_NDK                := /media/tu/Work/Android/android-ndk-r11c
#OE_ROOT 	        	:= /media/tu/Work/OSG_Earth
OSG_ANDROID_DIR		    := $(OE_ROOT)/OpenSceneGraph
OSGEARTH_ANDROID_DIR	:= $(OE_ROOT)/OsgEarth
THIRDPARTY_ANDROID_DIR	:= $(OE_ROOT)/ThirdParty-And
THIRDPARTY_LD_DIR       := $(THIRDPARTY_ANDROID_DIR)/obj/local/armeabi-v7a


LOCAL_ARM_NEON 			:= true
OSG_LIBDIR 				:= $(OSG_ANDROID_DIR)/build_and/obj/local/armeabi-v7a
OSGEARTH_LIBDIR 		:= $(OSGEARTH_ANDROID_DIR)/build_and/obj/local/armeabi-v7a
PNG_LIBDIR 			    := $(THIRDPARTY_LD_DIR)
TIFF_LIBDIR 			:= $(THIRDPARTY_LD_DIR)
GDAL_LIBDIR 			:= $(THIRDPARTY_LD_DIR)
GEOS_LIBDIR 			:= $(THIRDPARTY_LD_DIR)
PROJ_LIBDIR 			:= $(THIRDPARTY_LD_DIR)
CURL_LIBDIR 			:= $(THIRDPARTY_LD_DIR)
FREETYPE_LIBDIR 		:= $(THIRDPARTY_LD_DIR)
SQLITE_LIBDIR 			:= $(THIRDPARTY_LD_DIR)
ZLIB_LIBDIR 			:= $(THIRDPARTY_LD_DIR)

### Add all source file names to be included in lib separated by a whitespace

LOCAL_C_INCLUDES:= $(OSG_ANDROID_DIR)/include $(OSGEARTH_ANDROID_DIR)/src $(OSG_ANDROID_DIR)/build_and/include $(OSGEARTH_ANDROID_DIR)/build_and/build_include
LOCAL_CFLAGS    := -Werror -fno-short-enums -DANDROID -DOSGEARTH_LIBRARY_STATIC
LOCAL_CPPFLAGS  := -DOSG_LIBRARY_STATIC -DOSGEARTH_LIBRARY_STATIC -DANDROID

LOCAL_LDLIBS    := -llog -lGLESv3 -lz -ldl

LOCAL_SRC_FILES := osgNativeLib.cpp OsgMainApp.cpp OsgAndroidNotifyHandler.cpp

#LOCAL_C_INCLUDES += $(NDK_ROOT)/sources/cxx-stl/llvm-libc++/include
#LOCAL_C_INCLUDES += $(NDK_ROOT)/sources/cxx-stl/llvm-libc++/libs/armeabi-v7a/include

LOCAL_ALLOW_UNDEFINED_SYMBOLS := false

LOCAL_LDLIBS   += -L $(OSGEARTH_LIBDIR) \
-L $(OSGEARTH_LIBDIR) \
-losgdb_osgearth_arcgis \
-losgdb_osgearth_bing \
-losgdb_osgearth_bumpmap \
-losgdb_osgearth_cache_filesystem \
-losgdb_osgearth_colorramp \
-losgdb_osgearth_debug \
-losgdb_osgearth_detail \
-losgdb_earth \
-losgdb_zip \
-losgdb_osgearth_engine_mp \
-losgdb_osgearth_engine_rex \
-losgdb_osgearth_feature_elevation \
-losgdb_osgearth_feature_ogr \
-losgdb_osgearth_feature_tfs \
-losgdb_osgearth_feature_wfs \
-losgdb_osgearth_feature_xyz \
-losgdb_osgearth_featurefilter_intersect \
-losgdb_osgearth_featurefilter_join \
-losgdb_osgearth_gdal \
-losgdb_kml \
-losgdb_osgearth_label_annotation \
-losgdb_osgearth_mapinspector \
-losgdb_osgearth_mask_feature \
-losgdb_osgearth_model_feature_geom \
-losgdb_osgearth_model_simple \
-losgdb_osgearth_monitor \
-losgdb_osgearth_osg \
-losgdb_osgearth_scriptengine_javascript \
-losgdb_osgearth_sky_gl \
-losgdb_osgearth_sky_simple \
-losgdb_osgearth_skyview \
-losgdb_template \
-losgdb_osgearth_terrainshader \
-losgdb_osgearth_tileindex \
-losgdb_osgearth_tms \
-losgdb_osgearth_vdatum_egm84 \
-losgdb_osgearth_vdatum_egm96 \
-losgdb_osgearth_vdatum_egm2008 \
-losgdb_osgearth_viewpoints \
-losgdb_osgearth_vpb \
-losgdb_osgearth_wcs \
-losgdb_osgearth_wms \
-losgdb_osgearth_xyz \
-losgEarthAnnotation \
-losgEarthFeatures \
-losgEarthSymbology \
-losgEarthUtil \
-losgEarth \
-L $(OSG_LIBDIR) \
-losgdb_openflight \
-losgdb_curl \
-losgdb_obj \
-losgdb_shp \
-losgdb_rot \
-losgdb_scale \
-losgdb_trans \
-losgdb_jpeg \
-losgdb_png \
-losgdb_freetype \
-losgdb_osgterrain \
-losgdb_osg \
-losgdb_ive \
-losgdb_deprecated_osgviewer \
-losgdb_deprecated_osgvolume \
-losgdb_deprecated_osgtext \
-losgdb_deprecated_osgterrain \
-losgdb_deprecated_osgsim \
-losgdb_deprecated_osgshadow \
-losgdb_deprecated_osgparticle \
-losgdb_deprecated_osgfx \
-losgdb_deprecated_osganimation \
-losgdb_deprecated_osg \
-losgdb_serializers_osgvolume \
-losgdb_serializers_osgtext \
-losgdb_serializers_osgterrain \
-losgdb_serializers_osgsim \
-losgdb_serializers_osgshadow \
-losgdb_serializers_osgparticle \
-losgdb_serializers_osgmanipulator \
-losgdb_serializers_osgfx \
-losgdb_serializers_osganimation \
-losgdb_serializers_osg \
-losgViewer \
-losgVolume \
-losgTerrain \
-losgText \
-losgShadow \
-losgSim \
-losgParticle \
-losgManipulator \
-losgGA \
-losgFX \
-losgDB \
-losgAnimation \
-losgUtil \
-losg \
-lOpenThreads \
-L $(PNG_LIBDIR) \
-lpng \
-L $(FREETYPE_LIBDIR) \
-lft2 \
-L $(CURL_LIBDIR) \
-lcurl \
-L $(GDAL_LIBDIR) \
-lgdal \
-L $(GEOS_LIBDIR) \
-lgeos \
-L $(PROJ_LIBDIR) \
-lproj \
-L $(SQLITE_LIBDIR) \
-lsqlite3 \
-L $(ZLIB_LIBDIR) \
-lzlib

#-losgdb_osgearth_ocean_simple \
#-losgdb_osgearth_mbtiles \

LOCAL_LDLIBS += $(ANDROID_NDK)/sources/cxx-stl/gnu-libstdc++/4.9/libs/armeabi-v7a/libgnustl_static.a

include $(BUILD_SHARED_LIBRARY)
