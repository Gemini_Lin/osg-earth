#!/bin/sh

echo -n "是否要要覆盖工程文件 ? (y/n)  "

read is_overwrite

if [[ $is_overwrite = "y" ]]; then

    filepath=$(cd "$(dirname "$0")"; pwd)/package.tar.gz

    if [ -f "$filepath" ]; then
        tar zxvf package.tar.gz
    else
        echo "请将package拷贝到当前目录下，如果无，按任意键退出"
        read anykey
    fi
    exit 0
fi

echo -n "是否要对工程文件打包 ? (y/n)  "

read is_package

if [[ $is_package = "y" ]]; then
    tar zcvf package.tar.gz  OSGApi/OSGApi.xcodeproj OsgEarthApi/OsgEarthApi.xcodeproj OESDK.xcworkspace
fi


