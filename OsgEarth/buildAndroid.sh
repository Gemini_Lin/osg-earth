#!/bin/bash

# This must match the path to your osg-android build
BASE_PATH=$(pwd)/../OpenSceneGraph
BASE_OBJ=$BASE_PATH/build_and

mkdir -p build_and && cd build_and
echo $ANDROID_NDK
cmake .. \
-DANDROID_NDK=$ANDROID_NDK \
-DOSG_DIR:PATH="$BASE_PATH" \
-DOSG_INCLUDE_DIR:PATH="$BASE_PATH/include" \
-DOSG_GEN_INCLUDE_DIR:PATH="$BASE_OBJ/include" \
-DOSGEARTH_BUILD_PLATFORM_ANDROID:BOOL=ON \
-DANDROID_STL=gnustl_static \
-DANDROID_ABI="armeabi-v7a" \
-DANDROID_PLATFORM=21 \
-DOSGEARTH_BUILD_SHARED_LIBS:BOOL=OFF
echo ">>>>> Auto Generate Shader Files <<<<<"
CURRENT_PATH=$(pwd)
cmake -P $CURRENT_PATH/src/osgEarth/ConfigureShaders.cmake

cmake -P $CURRENT_PATH/src/osgEarthFeatures/ConfigureShaders.cmake

cmake -P $CURRENT_PATH/src/osgEarthDrivers/bumpmap/ConfigureShaders.cmake

cmake -P $CURRENT_PATH/src/osgEarthDrivers/detail/ConfigureShaders.cmake

cmake -P $CURRENT_PATH/src/osgEarthDrivers/engine_rex/ConfigureShaders.cmake

cmake -P $CURRENT_PATH/src/osgEarthDrivers/sky_simple/ConfigureShaders.cmake

cmake -P $CURRENT_PATH/src/osgEarthDrivers/engine_mp/ConfigureShaders.cmake

cmake -P $CURRENT_PATH/src/osgEarthUtil/ConfigureShaders.cmake
echo ">>>>>    Build End.    <<<<<"
#make

