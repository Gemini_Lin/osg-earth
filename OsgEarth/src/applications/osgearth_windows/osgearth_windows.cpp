/* -*-c++-*- */
/* osgEarth - Geospatial SDK for OpenSceneGraph
* Copyright 2019 Pelican Mapping
* http://osgearth.org
*
* osgEarth is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
* IN THE SOFTWARE.
*
* You should have received a copy of the GNU Lesser General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
*/

#include <osgViewer/CompositeViewer>
#include <osgViewer/Viewer>
#include <osgEarth/Notify>
#include <osgEarthUtil/EarthManipulator>
#include <osgEarthUtil/ExampleResources>
#include <osgEarth/MapNode>
#include <osgEarth/ThreadingUtils>
#include <osgEarth/Metrics>
#include <osgEarthUtil/Sky>
#include <osgEarthUtil/Shadowing>
#include <osgEarthUtil/LogarithmicDepthBuffer>
#include <osgEarthAnnotation/ImageOverlay>

#include <iostream>

#include <osg/os_utils>
#include <osg/CullFace>
#include <osg/BlendFunc>
#include <osg/PositionAttitudeTransform>
#include <osgEarthUtil/AutoClipPlaneHandler>
#include <osgEarthFeatures/GeometryUtils>

#include <osgEarthUtil/GeodeticGraticule>

#define LC "[viewer] "

using namespace osgEarth;
using namespace osgEarth::Util;

int
usage(const char* name)
{
    OE_NOTICE 
        << "\nUsage: " << name << " file.earth"
        << "\n          --views [num] : Number of windows to open"
        << "\n          --shared      : Use a shared graphics context"
        << "\n"
        << MapNodeHelper().usage() << std::endl;

    return 0;
}

// USE_OSGPLUGIN(earth)



int
main(int argc, char** argv)
{
#if 0
    osg::ArgumentParser arguments(&argc,argv);

    // help?
    if ( arguments.read("--help") )
        return usage(argv[0]);

    int numViews = 1;
    arguments.read("--views", numViews);

    bool sharedGC;
    sharedGC = arguments.read("--shared");

    // create a viewer:
    osgViewer::Viewer viewer(arguments);
    //viewer.setThreadingModel(viewer.SingleThreaded);

    osg::Node* node = MapNodeHelper().load(arguments, &viewer);
    if (!node)
        return usage(argv[0]);
    // osg::ref_ptr<osgEarth::Util::MapNode> mapnode = osgEarth::Util::MapNode::findMapNode(node);
    // osgEarth::setNotifyLevel(osg::DEBUG_FP);

    // if( !mapnode.valid() )
    // {
    //     OSG_WARN << "Loaded scene graph does not contain a MapNode - aborting" << std::endl;
    //     return -1;
    // }

    viewer.setSceneData( node );
    // osgEarth::Util::ShadowCaster *caster = new osgEarth::Util::ShadowCaster();

    // osgEarth::Util::SkyNode *skynode = osgEarth::Util::SkyNode::create(mapnode);
    // static_cast<osg::Group*>(node)->addChild(skynode);
    // static_cast<osg::Group*>(node)->addChild(caster);
    // skynode->setAtmosphereVisible(true);
    // 设置时间;
	// osgEarth::DateTime dateTime(2020, 10, 27, 11);
	// osgEarth::Util::Ephemeris* ephemeris = new osgEarth::Util::Ephemeris;
    // skynode->setEphemeris(ephemeris);

    // skynode->setDateTime(dateTime);
    // skynode->setLighting(osg::StateAttribute::ON);
    int size = 500;
    // caster->addChild(mapnode);
    int cores = OpenThreads::GetNumberOfProcessors();
    osg::DisplaySettings::instance()->setNumOfDatabaseThreadsHint( osg::clampAbove( cores, 2 ) );
    osg::DisplaySettings::instance()->setNumOfHttpDatabaseThreadsHint( osg::clampAbove( cores/2, 1 ) ); 
    
    uint16_t _numDatabaseThreadsHint = 0;
    uint16_t _numHttpDatabaseThreadsHint = 0;
    osg::getEnvVar("OSG_NUM_DATABASE_THREADS", _numDatabaseThreadsHint);

    osg::getEnvVar("OSG_NUM_HTTP_DATABASE_THREADS", _numHttpDatabaseThreadsHint);
    std::cout << _numDatabaseThreadsHint << " " << _numHttpDatabaseThreadsHint << std::endl;
    // for(int i=0; i<numViews; ++i)
    // {
    //     osgViewer::View* view = new osgViewer::View();
    //     // skynode->attach(view);
    //     // caster->setLight(view->getLight());

    //     int width = sharedGC? size*numViews : size;
    //     view->setUpViewInWindow(10+(i*size+30), 10, width, size);

    //     osg::ref_ptr<osgEarth::Util::EarthManipulator> earthManip = new osgEarth::Util::EarthManipulator;

    //     view->setCameraManipulator(earthManip);//必须在setViewpoint之前

    //     osgEarth::Viewpoint vp( "", 116.390777893431, 39.9153372188753, 56.3822565171868, -2.07733717805721, -32.9309817308407, 2287.68298543071 );
    
    //     //earthManip->setViewpoint( vp, 0 );
    //     // earthManip->setHomeViewpoint(vp);
        
    //     view->setSceneData(node);
    //     view->getDatabasePager()->setUnrefImageDataAfterApplyPolicy( true, false );
    //     if (sharedGC)
    //     {
    //         view->getCamera()->setViewport(i*size, 0, size, size);
    //         view->getCamera()->setProjectionMatrixAsPerspective(45, 1, 1, 10);
    //         view->getCamera()->setName(Stringify()<<"View "<<i);
    //     }
    //     MapNodeHelper().configureView(view);
    //     viewer.addView(view);
    // }

    // if (sharedGC)
    // {
    //     for(int i=1; i<numViews; ++i)
    //     {
    //         osgViewer::View* view = viewer.getView(i);
    //         view->getCamera()->setGraphicsContext(viewer.getView(0)->getCamera()->getGraphicsContext());
    //     }
    // }

    return viewer.run();
#else
    osg::ArgumentParser arguments(&argc,argv);

    // help?
    if ( arguments.read("--help") )
        return usage(argv[0]);

    std::cout << argv[1] << std::endl;

    // create a viewer:
    osgViewer::Viewer viewer(arguments);

    // Tell the database pager to not modify the unref settings
    viewer.getDatabasePager()->setUnrefImageDataAfterApplyPolicy( true, false );

    // thread-safe initialization of the OSG wrapper manager. Calling this here
    // prevents the "unsupported wrapper" messages from OSG
    osgDB::Registry::instance()->getObjectWrapperManager()->findWrapper("osg::Image");

    // disable the small-feature culling
    // viewer.getCamera()->setSmallFeatureCullingPixelSize(-1.0f);

    // set a near/far ratio that is smaller than the default. This allows us to get
    // closer to the ground without near clipping. If you need more, use --logdepth
    // viewer.getCamera()->setNearFarRatio(0.00001);
    viewer.getCamera()->setNearFarRatio(0.00001);
    
    // osgEarth::setNotifyLevel(osg::DEBUG_FP);
 
    osgEarth::Viewpoint vp( "", 116.315, 39.9834, 56.3822565171868, -2.07733717805721, -32.9309817308407, 150.68298543071 );


    EarthManipulator* manip = new EarthManipulator(arguments);
    
    // //add go to comment
    manip->getSettings()->bindMouseClick(
        EarthManipulator::ACTION_GOTO,
        osgGA::GUIEventAdapter::LEFT_MOUSE_BUTTON,
        osgGA::GUIEventAdapter::MODKEY_SHIFT);

    viewer.setCameraManipulator( manip );

     osg::GraphicsContext::WindowingSystemInterface* wsi = osg::GraphicsContext::getWindowingSystemInterface();
    unsigned width, height;
     // query the screen size.
    osg::GraphicsContext::ScreenIdentifier si;
    wsi->getScreenResolution( si, width, height );
    unsigned b = 50;

    // manip->setHomeViewpoint(vp);

    // load an earth file, and support all or our example command-line options
    // and earth file <external> tags  
    osg::Group *root = new osg::Group();  

    auto node = MapNodeHelper().load(arguments, &viewer);
    root->addChild(node);
    MapNode* mapNode = MapNode::findMapNode(node);
    //viewer.getCamera()->addCullCallback(new osgEarth::Util::AutoClipPlaneCullCallback(mapNode));

    viewer.getCamera()->setSmallFeatureCullingPixelSize(-1.0f);
    
    int cores = OpenThreads::GetNumberOfProcessors();
    osg::DisplaySettings::instance()->setNumOfDatabaseThreadsHint( osg::clampAbove( cores, 2 ) );
    osg::DisplaySettings::instance()->setNumOfHttpDatabaseThreadsHint( osg::clampAbove( cores/2, 1 ) ); 

    viewer.getCamera()->setProjectionMatrixAsPerspective(45.0f, float(4096 / 2304), 0.1f, 10000.0f);
    //开启后 纹理会消失 原因暂不明
    // osgEarth::Util::LogarithmicDepthBuffer buf;
    // buf.install( viewer.getCamera() );

    // osg::Node *model = osgDB::readNodeFile("/media/tu/Work/Datas/OpenSceneGraph-Data/cow.osg");
    // osg::Node *model = osgDB::readNodeFile("/media/tu/Work/Datas/model/beiqing_20201203_2/CM_CN_BEIJING_2477027052.dae");
    // const osgEarth::SpatialReference* geoSRS = mapNode->getMapSRS()->getGeodeticSRS();
    // if( NULL != model )
    // {
	//     model->getOrCreateStateSet()->setMode(GL_LIGHTING, osg::StateAttribute::OFF);
    //     model->getOrCreateStateSet()->setMode(GL_RESCALE_NORMAL, osg::StateAttribute::ON);
        
    //     //开启融合操作模式
	//     model->getOrCreateStateSet()->setMode(GL_BLEND, osg::StateAttribute::ON);

    //     osg::BlendFunc *fn = new osg::BlendFunc();
    //     fn->setFunction(osg::BlendFunc::SRC_ALPHA, osg::BlendFunc::ONE_MINUS_SRC_ALPHA);
    //     model->getOrCreateStateSet()->setAttributeAndModes(fn, osg::StateAttribute::ON);

    //     const int sclen = 1;
       
    //     osg::Matrix Lmatrix;
    //     // // 放大一些，方便看到
	// 	Lmatrix.preMult(osg::Matrix::scale(osg::Vec3(sclen, sclen, sclen)));
	// 	geoSRS->getEllipsoid()->computeLocalToWorldTransformFromLatLongHeight(osg::DegreesToRadians(39.9834), osg::DegreesToRadians(116.315), 2.5, Lmatrix);
		
	// 	osg::MatrixTransform* mt = new osg::MatrixTransform;
	// 	mt->setMatrix(Lmatrix);
	// 	mt->addChild(model);
    //     node->addChild(mt);
    // }
    // osg::ref_ptr<osg::Image> image = osgDB::readRefImageFile( "/media/tu/Work/Sources/osgearth/fsosgearth/osgearth/build/data/test_bump.png");

    // if (image.valid())
    // {
    //     osg::ref_ptr<osgEarth::Annotation::ImageOverlay> imageOverlay = new osgEarth::Annotation::ImageOverlay(mapNode, image.get());

    //     mapNode->addChild( imageOverlay );

    //     imageOverlay->setCorners(osg::Vec2d(-84.0,24), 
    //                              osg::Vec2d(-80.0,24), 
    //                              osg::Vec2d(-84,30), 
    //                              osg::Vec2d(-80.0,30));
    // }

    // GeodeticGraticule* graticule = new GeodeticGraticule();
    // mapNode->getMap()->addLayer(graticule);

    if ( node )
    {
        viewer.setSceneData( root );

        Metrics::run(viewer);
    }
    else
    {
        return usage(argv[0]);
    }

#endif
}
