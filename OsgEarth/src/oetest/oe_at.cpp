/* -*-c++-*- */
/* osgEarth - Geospatial SDK for OpenSceneGraph
* Copyright 2019 Pelican Mapping
* http://osgearth.org
*
* osgEarth is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
* IN THE SOFTWARE.
*
* You should have received a copy of the GNU Lesser General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
*/

#include <osgViewer/CompositeViewer>
#include <osgViewer/Viewer>
#include <osgEarth/Notify>
#include <osgEarthUtil/EarthManipulator>
#include <osgEarthUtil/ExampleResources>
#include <osgEarth/MapNode>
#include <osgEarth/ThreadingUtils>
#include <osgEarth/Metrics>
#include <osgEarthUtil/Sky>
#include <osgEarthUtil/Shadowing>
#include <osgEarthUtil/LogarithmicDepthBuffer>
#include <osgEarthAnnotation/ImageOverlay>

#include <iostream>

#include <osg/os_utils>
#include <osg/CullFace>
#include <osg/BlendFunc>
#include <osg/PositionAttitudeTransform>
#include <osgEarthUtil/AutoClipPlaneHandler>
#include <osgEarthFeatures/GeometryUtils>

#define LC "[viewer] "

using namespace osgEarth;
using namespace osgEarth::Util;

int
usage(const char* name)
{
    OE_NOTICE 
        << "\nUsage: " << name << " file.earth"
        << "\n          --views [num] : Number of windows to open"
        << "\n          --shared      : Use a shared graphics context"
        << "\n"
        << MapNodeHelper().usage() << std::endl;

    return 0;
}

// USE_OSGPLUGIN(earth)

int main(int argc, char** argv)
{
    osg::ArgumentParser arguments(&argc,argv);

    // help?
    if ( arguments.read("--help") )
        return usage(argv[0]);

    std::cout << argv[1] << std::endl;

    // create a viewer:
    osgViewer::Viewer viewer(arguments);

    // Tell the database pager to not modify the unref settings
    viewer.getDatabasePager()->setUnrefImageDataAfterApplyPolicy( true, false );

    // thread-safe initialization of the OSG wrapper manager. Calling this here
    // prevents the "unsupported wrapper" messages from OSG
    osgDB::Registry::instance()->getObjectWrapperManager()->findWrapper("osg::Image");

    // disable the small-feature culling
    // viewer.getCamera()->setSmallFeatureCullingPixelSize(-1.0f);

    // set a near/far ratio that is smaller than the default. This allows us to get
    // closer to the ground without near clipping. If you need more, use --logdepth
    // viewer.getCamera()->setNearFarRatio(0.00001);
    viewer.getCamera()->setNearFarRatio(0.00001);
    
    // osgEarth::setNotifyLevel(osg::DEBUG_FP);
 
    osgEarth::Viewpoint vp( "", 116.315, 39.9834, 56.3822565171868, -2.07733717805721, -32.9309817308407, 150.68298543071 );


    EarthManipulator* manip = new EarthManipulator(arguments);
    
    // //add go to comment
    manip->getSettings()->bindMouseClick(
        EarthManipulator::ACTION_GOTO,
        osgGA::GUIEventAdapter::LEFT_MOUSE_BUTTON,
        osgGA::GUIEventAdapter::MODKEY_SHIFT);

    viewer.setCameraManipulator( manip );

     osg::GraphicsContext::WindowingSystemInterface* wsi = osg::GraphicsContext::getWindowingSystemInterface();
    unsigned width, height;
     // query the screen size.
    osg::GraphicsContext::ScreenIdentifier si;
    wsi->getScreenResolution( si, width, height );
    unsigned b = 50;

    // manip->setHomeViewpoint(vp);

    // load an earth file, and support all or our example command-line options
    // and earth file <external> tags  
    osg::Group *root = new osg::Group();  

    auto node = MapNodeHelper().load(arguments, &viewer);
    root->addChild(node);
    MapNode* mapNode = MapNode::findMapNode(node);
    //viewer.getCamera()->addCullCallback(new osgEarth::Util::AutoClipPlaneCullCallback(mapNode));

    viewer.getCamera()->setSmallFeatureCullingPixelSize(-1.0f);
    
    int cores = OpenThreads::GetNumberOfProcessors();
    osg::DisplaySettings::instance()->setNumOfDatabaseThreadsHint( osg::clampAbove( cores, 2 ) );
    osg::DisplaySettings::instance()->setNumOfHttpDatabaseThreadsHint( osg::clampAbove( cores/2, 1 ) ); 

    viewer.getCamera()->setProjectionMatrixAsPerspective(45.0f, float(4096 / 2304), 0.1f, 10000.0f);

    if ( node )
    {
        viewer.setSceneData( root );

        Metrics::run(viewer);
    }
    else
    {
        return usage(argv[0]);
    }
}
