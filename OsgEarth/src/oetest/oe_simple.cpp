/* -*-c++-*- */
/* osgEarth - Geospatial SDK for OpenSceneGraph
* Copyright 2019 Pelican Mapping
* http://osgearth.org
*
* osgEarth is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
* IN THE SOFTWARE.
*
* You should have received a copy of the GNU Lesser General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
*/

#include <iostream>

#include <osg/Material>
#include <osg/BlendFunc>
#include <osg/MatrixTransform>

#include <osgViewer/Viewer>
#include <osgViewer/ViewerEventHandlers>

#include <osgEarth/MapNode>
#include <osgEarth/Lighting>
#include <osgEarth/ImageLayer>
#include <osgEarth/ElevationLayer>


#include <osgEarthUtil/Sky>
#include <osgEarthUtil/EarthManipulator>

#include <osgEarthDrivers/xyz/XYZOptions>
#include <osgEarthDrivers/gdal/GDALOptions>
// #include <osgEarthDrivers/
#include <osgGA/StateSetManipulator>

using namespace osgEarth;
using namespace osgEarth::Util;

typedef std::map<std::string, std::string> URLContainer;

URLContainer xyzimgurls = { 
                                    {"smap" ,"http://smap.navinfo.com/gateway/smap-raster-map/raster/basemap/tile?specId=902157&ak=ff0d2a9244c609d1b4115183e&layer=navinfo_world&z={z}&x={x}&y={y}"},
                                    {"gaode","http://webst0[1234].is.autonavi.com/appmaptile?style=6&x={x}&y={y}&z={z}"}
};

URLContainer gdalimgurls = {
    {"world","/media/tu/Work/OSG_Earth/OsgEarth/data/world.tif"}
};

URLContainer eleurls = {
    {"mapbox","http://api.mapbox.com/v4/mapbox.terrain-rgb/{z}/{x}/{y}.pngraw?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4M29iazA2Z2gycXA4N2pmbDZmangifQ.-g_vE53SD2WrJ6tFX7QHmA"}
};


void createXYZImageLayer(const std::string &imgname,  osgEarth::Map *map)
{
    auto it = xyzimgurls.find(imgname);
    if(xyzimgurls.end() == it) return;
    osgEarth::Drivers::XYZOptions xyzop;
    xyzop.url() = it->second;
    xyzop.profile() = std::move(osgEarth::ProfileOptions("spherical-mercator"));
    xyzop.getConfig().key() = "xyz";
    osgEarth::ImageLayer *smaplayer = new osgEarth::ImageLayer(it->first,xyzop);
    smaplayer->options().noDataImageFilename() = "/media/tu/Work/OSG_Earth/OsgEarth/data/airport.png";
    map->addLayer(smaplayer);
    // smaplayer->getTileSource()->getDataExtents().data()->bounds();
}

void createGdalImageLayer(const std::string &imgname, osgEarth::Map *map)
{
    auto it = gdalimgurls.find(imgname);
    if(gdalimgurls.end() == it) return;
    osgEarth::Drivers::GDALOptions gdalop;
    gdalop.url() = it->second;
    gdalop.profile() = std::move(osgEarth::ProfileOptions("spherical-mercator"));
    gdalop.getConfig().key() = "gdal";
    osgEarth::ImageLayer *gdallayer = new osgEarth::ImageLayer(it->first, gdalop);
    map->addLayer(gdallayer);
}

void createElevationLayer(const std::string &elname, osgEarth::Map *map)
{
    auto it = eleurls.find(elname);
    if(eleurls.end() == it) return;
    osgEarth::Drivers::XYZOptions xyzop;
    xyzop.url() = it->second;
    xyzop.profile() = std::move(osgEarth::ProfileOptions("spherical-mercator"));
    xyzop.getConfig().key() = "xyz";
    if(elname == "mapbox")
        xyzop.elevationEncoding() = "mapbox";
    osgEarth::ElevationLayerOptions layerOpt(xyzop);
    layerOpt.minLevel() = 12;
    osgEarth::ElevationLayer *elelayer = new osgEarth::ElevationLayer(it->first,layerOpt);
    map->addLayer(elelayer);
}

int main(int argc, char** argv)
{
    std::string filepath;
    if(argc < 2)
    {
        filepath = "/home/tu/Documents/earthfiles/smapraster.earth";
    }
    else
    {
        filepath = std::string(argv[1]);
    }
    // create a viewer:
    osgViewer::Viewer viewer;
    
    osg::ref_ptr<osg::Group> root = new osg::Group();
    osgEarth::Map *map = new osgEarth::Map();
    // createGdalImageLayer("world",map);
    createXYZImageLayer("gaode",map);
    createElevationLayer("mapbox",map);
    osgEarth::MapNodeOptions mapoptions;
    osgEarth::TerrainOptions teroptions;
    teroptions.setDriver("rex");
    teroptions.tileSize() = 21;
    teroptions.minTileRangeFactor() = 13;
    teroptions.attenuationDistance() = 1e4;
    mapoptions.setTerrainOptions(teroptions);
    osg::ref_ptr<osg::Node> mpnode = new osgEarth::MapNode(map, mapoptions);

    EarthManipulator* manip = new EarthManipulator();
    osgEarth::Util::EarthManipulator::ActionOptions options;

    manip->getSettings()->setMinMaxDistance(0.1,1.5e7);
	manip->getSettings()->bindTwist(osgEarth::Util::EarthManipulator::ACTION_NULL, options);
	manip->getSettings()->bindMultiDrag(osgEarth::Util::EarthManipulator::ACTION_NULL, options);
	manip->getSettings()->setMinMaxPitch(-90.0, 90.0);
    viewer.setCameraManipulator( manip );

    viewer.addEventHandler(new osgViewer::StatsHandler());
    viewer.addEventHandler(new osgViewer::WindowSizeHandler());
    viewer.addEventHandler(new osgGA::StateSetManipulator(viewer.getCamera()->getOrCreateStateSet()));
    viewer.getCamera()->setLODScale(viewer.getCamera()->getLODScale() * 1.5f);
      osg::GraphicsContext::WindowingSystemInterface* wsi = osg::GraphicsContext::getWindowingSystemInterface();
    unsigned width, height;
    //  // query the screen size.
    osg::GraphicsContext::ScreenIdentifier si;
    wsi->getScreenResolution( si, width, height );
 
    osgEarth::MapNode *globenode = osgEarth::MapNode::get(mpnode.get());

    osgEarth::Util::SkyOptions sOptions;
	// sOptions.ambient() = 0.03;
	osgEarth::Util::SkyNode* pSkyNode = osgEarth::Util::SkyNode::create(sOptions, globenode);

    osgEarth::DateTime dateTime(2021, 4, 27, 8);
	osgEarth::Util::Ephemeris* ephemeris = new osgEarth::Util::Ephemeris;
    pSkyNode->setName("first");
    pSkyNode->setDateTime(dateTime);
    pSkyNode->setEphemeris(ephemeris);
    pSkyNode->setLighting(true);
    pSkyNode->attach(&viewer,0);
 
    pSkyNode->addChild(globenode);
    root->addChild(pSkyNode);

    osg::Material* material = new osg::Material();
    material->setAmbient(osg::Material::FRONT, osg::Vec4(0.4,0.4,0.4,1.0));
    material->setSpecular(osg::Material::FRONT, osg::Vec4(0.1,0.1,0.1,0.2));
    material->setUpdateCallback(new osgEarth::MaterialCallback());
    globenode->getOrCreateStateSet()->setAttribute(material);

    viewer.getCamera()->setNearFarRatio(0.00001);
    viewer.getCamera()->setProjectionMatrixAsPerspective(45.0f, width / (float)height, 0.1f, 5000.0f);
    viewer.setSceneData(root);

    int cores = OpenThreads::GetNumberOfProcessors();
    osg::DisplaySettings::instance()->setNumOfDatabaseThreadsHint( osg::clampAbove( cores, 2 ) );
    osg::DisplaySettings::instance()->setNumOfHttpDatabaseThreadsHint( osg::clampAbove( cores/2, 1 ) ); 


    osg::Node *model = osgDB::readNodeFile("/media/tu/Work/Datas/model/beiqing_20201203_2/CM_CN_BEIJING_2477027052.dae");
    const osgEarth::SpatialReference* geoSRS = globenode->getMapSRS()->getGeodeticSRS();
    if( NULL != model )
    {
	    model->getOrCreateStateSet()->setMode(GL_LIGHTING, osg::StateAttribute::OFF);
        model->getOrCreateStateSet()->setMode(GL_RESCALE_NORMAL, osg::StateAttribute::ON);
        
        //开启融合操作模式
	    model->getOrCreateStateSet()->setMode(GL_BLEND, osg::StateAttribute::ON);

        osg::BlendFunc *fn = new osg::BlendFunc();
        fn->setFunction(osg::BlendFunc::SRC_ALPHA, osg::BlendFunc::ONE_MINUS_SRC_ALPHA);
        model->getOrCreateStateSet()->setAttributeAndModes(fn, osg::StateAttribute::ON);

        const int sclen = 1;
       
        osg::Matrix Lmatrix;
        // // 放大一些，方便看到
		Lmatrix.preMult(osg::Matrix::scale(osg::Vec3(sclen, sclen, sclen)));
		geoSRS->getEllipsoid()->computeLocalToWorldTransformFromLatLongHeight(osg::DegreesToRadians(39.9834), osg::DegreesToRadians(116.315), 2.5, Lmatrix);
		
		osg::MatrixTransform* mt = new osg::MatrixTransform;
		mt->setMatrix(Lmatrix);
		mt->addChild(model);
        globenode->addChild(mt);
    }
    osgEarth::Viewpoint vp( "", 116.315, 39.9834, 56.3822565171868, -2.07733717805721, -32.9309817308407, 150.68298543071 );
    manip->setHomeViewpoint(vp);
    viewer.realize();
    return viewer.run();
}
