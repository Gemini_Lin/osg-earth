message("#############################################")
message("############# build test ####################")
message("#############################################")

INCLUDE_DIRECTORIES(${OSG_INCLUDE_DIRS} )
SET(TARGET_COMMON_LIBRARIES
    osgEarth
    osgEarthFeatures
    osgEarthUtil
    osgEarthSymbology
    osgEarthAnnotation
)
SET(TARGET_LIBRARIES_VARS OSG_LIBRARY OSGDB_LIBRARY OSGUTIL_LIBRARY OSGVIEWER_LIBRARY OPENTHREADS_LIBRARY)

function(build_target)
    message("---> Build Example Name : " ${ARGV0})
    SET(TARGET_TARGETNAME ${ARGV0})
    SET(TARGET_SRC
        ${ARGV0}.cpp 
    )
    SETUP_APPLICATION(${ARGV0})
endfunction()

build_target(oe_simple)
#build_target(oe_at)