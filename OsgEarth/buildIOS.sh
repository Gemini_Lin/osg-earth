#!/bin/sh

#  build_osg_earth_ios.sh
#  FSDemo01
#
#  Created by workpc on 2020/9/24.
#  Copyright © 2020 workpc. All rights reserved.

rm -r -f build_ios
mkdir build_ios
cd build_ios

# 相对路径
#export Third_Path=../../../../3rdParty
#
#export OSG_Build_Path=../OpenSceneGraph

#绝对路径
export ROOT="$(pwd)/../../"
export Third_Path="$ROOT/3rdParty"
export OSG_Root="$ROOT/OpenSceneGraph"
export OSG_Build_Path="$OSG_Root/build_ios"

# 获取当前电脑中xcode对应的ios sdk版本
ios_sdk_version=`xcodebuild -version -sdk iphoneos SDKVersion`
echo "当前xcode对应的iphoneos SDKVersion = ${ios_sdk_version}"

cmake ../ -G Xcode -DOSG_BUILD_PLATFORM_IPHONE:BOOL=ON \
-DIPHONE_ENABLE_BITCODE:BOOL=OFF \
-DOSG_DIR:PATH="$OSG_Build_Path" \
-DOSG_INCLUDE_DIR:PATH="$OSG_Root/include" \
-DOSG_GEN_INCLUDE_DIR:PATH="$OSG_Build_Path/include" \
-DIPHONE_SDKVER="${ios_sdk_version}" \
-DIPHONE_VERSION_MIN="10.0" \
-DOSGEARTH_BUILD_PLATFORM_IPHONE:BOOL=ON \
-DOSGEARTH_BUILD_SHARED_LIBS:BOOL=OFF \
-DCURL_INCLUDE_DIR:PATH="$Third_Path/curl-ios-device/include" \
-DCURL_LIBRARY:PATH="$Third_Path/curl-ios-device/lib/libcurl.a" \
-DGDAL_INCLUDE_DIR:PATH="$Third_Path/gdal-ios-device/include" \
-DGDAL_LIBRARY:PATH="$Third_Path/gdal-ios-device/lib/libgdal.a" \
-DGEOS_INCLUDE_DIR:PATH="$Third_Path/geos-ios-device/include" \
-DGEOS_LIBRARY:PATH="$Third_Path/geos-ios-device/lib/libGEOS_3.9.1.a" \
-DPROJ_LIBRARY:PATH="$Third_Path/proj4-ios-device/lib/libproj.a" \
-DFREETYPE_LIBRARY:PATH="$Third_Path/freetype-ios-universal/lib/libFreetype2.a" \
-DPNG_LIBRARY:PATH="$Third_Path/png-ios-device/lib/libpng.a" \
-DOSGEARTH_BUILD_APPLICATION_BUNDLES:BOOL=OFF \
-DDYNAMIC_OSGEARTH:BOOL=OFF \
-DOSGEARTH_USE_QT:BOOL=OFF \
-DOPENTHREADS_LIBRARY:PATH="$OSG_Build_Path/lib/libOpenThreadsd.a" \
-DOSGDB_LIBRARY:PATH="$OSG_Build_Path/lib/libosgDBd.a" \
-DOSGFX_LIBRARY:PATH="$OSG_Build_Path/lib/libosgFXd.a" \
-DOSGGA_LIBRARY:PATH="$OSG_Build_Path/lib/libosgGAd.a" \
-DOSGMANIPULATOR_LIBRARY:PATH="$OSG_Build_Path/lib/libosgManipulatord.a" \
-DOSGSHADOW_LIBRARY:PATH="$OSG_Build_Path/lib/libosgShadowd.a" \
-DOSGSIM_LIBRARY:PATH="$OSG_Build_Path/lib/libosgSimd.a" \
-DOSGTERRAIN_LIBRARY:PATH="$OSG_Build_Path/lib/libosgTerraind.a" \
-DOSGTEXT_LIBRARY:PATH="$OSG_Build_Path/lib/libosgTextd.a" \
-DOSGUTIL_LIBRARY:PATH="$OSG_Build_Path/lib/libosgUtild.a" \
-DOSGVIEWER_LIBRARY:PATH="$OSG_Build_Path/lib/libosgViewerd.a" \
-DOSGWIDGET_LIBRARY:PATH="$OSG_Build_Path/lib/libosgWidgetd.a" \
-DOSG_LIBRARY:PATH="$OSG_Build_Path/lib/libosgd.a" \
-DOSGPARTICLE_LIBRARY:PATH="$OSG_Build_Path/lib/libosgParticled.a"


