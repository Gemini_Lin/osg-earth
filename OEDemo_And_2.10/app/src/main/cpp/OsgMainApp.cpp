#include "OsgMainApp.hpp"

<<<<<<< HEAD
#include <osgViewer/GraphicsWindow>
#include <osgEarth/MapNode>
#include <osgEarth/Registry>
#include <osgEarth/AndroidCapabilities>
=======
#include <osgEarth/MapNode>
>>>>>>> 5367b4b299b5887bbf7168f96b2438d89b3028dc
#include <osgEarthUtil/Sky>
#include <osgEarthUtil/EarthManipulator>

#include "osgPlugins.h"

OsgMainApp::OsgMainApp()
{
    _initialized = false;

}
OsgMainApp::~OsgMainApp()
{

}

//Initialization function
void OsgMainApp::initOsgWindow(int x,int y,int width,int height)
{
    __android_log_write(ANDROID_LOG_ERROR, "OSGANDROID",
            "Initializing geometry");

    //Pending
    OsgAndroidNotifyHandler* _notifyHandler = new OsgAndroidNotifyHandler();
<<<<<<< HEAD
    _notifyHandler->setTag("OsgEarth");
    osg::setNotifyHandler(_notifyHandler);
    osg::getNotifyHandler()->notify(osg::WARN,"world");
    //osgEarth::Registry::instance()->setCapabilities(new osgEarth::AndroidCapabilities());

    _viewer = new osgViewer::Viewer();

    _viewer->getEventQueue()->setGraphicsContext(_viewer->setUpViewerAsEmbeddedInWindow(0, 0, width, height));

	_viewer->getCamera()->setViewport(new osg::Viewport(0, 0, width, height));
	_viewer->getCamera()->setClearMask(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	_viewer->getCamera()->setClearColor(osg::Vec4(0.0f, 0.0f, 0.5f, 1.0f));
	//_viewer->getEventQueue()->getCurrentEventState()->setMouseYOrientation(osgGA::GUIEventAdapter::Y_INCREASING_UPWARDS);
	_viewer->getCamera()->setNearFarRatio(0.00002f);
	_viewer->setThreadingModel(osgViewer::ViewerBase::SingleThreaded);
	//_viewer->getCamera()->setLODScale(_viewer->getCamera()->getLODScale() * 1.5f);
=======
    _notifyHandler->setTag("Osg Viewer");
    osg::setNotifyHandler(_notifyHandler);

    _viewer = new osgViewer::Viewer();

    _viewer->setUpViewerAsEmbeddedInWindow(0, 0, width, height);
	_viewer->getCamera()->setViewport(new osg::Viewport(0, 0, width, height));
	_viewer->getCamera()->setClearMask(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	_viewer->getCamera()->setClearColor(osg::Vec4(0.0f, 0.0f, 0.5f, 1.0f));
	_viewer->getCamera()->setProjectionMatrixAsPerspective(45.0f, (float)(width / height), 0.1f, 1000.0f);
	_viewer->getEventQueue()->getCurrentEventState()->setMouseYOrientation(osgGA::GUIEventAdapter::Y_INCREASING_UPWARDS);
	_viewer->getCamera()->setNearFarRatio(0.00002f);
	_viewer->setThreadingModel(osgViewer::ViewerBase::SingleThreaded);
	_viewer->getCamera()->setLODScale(_viewer->getCamera()->getLODScale() * 1.5f);
>>>>>>> 5367b4b299b5887bbf7168f96b2438d89b3028dc
	_viewer->getDatabasePager()->setIncrementalCompileOperation(new osgUtil::IncrementalCompileOperation());
	_viewer->getDatabasePager()->setDoPreCompile(true);
	_viewer->getDatabasePager()->setTargetMaximumNumberOfPageLOD(0);
	_viewer->getDatabasePager()->setUnrefImageDataAfterApplyPolicy(true, true);
	_viewer->setCameraManipulator(new osgEarth::Util::EarthManipulator());
<<<<<<< HEAD
    //_viewer->getCamera()->setProjectionMatrixAsPerspective(30.0f, width / (float)height, 0.1f, 5000.0f);
=======
	// _viewer->setRunFrameScheme( osgViewer::ViewerBase::ON_DEMAND );

>>>>>>> 5367b4b299b5887bbf7168f96b2438d89b3028dc
	osg::Node* node = osgDB::readNodeFile(_filepath);

	if(!node) {
		OSG_ALWAYS << "Unable to load an earth file from the command line." << std::endl;
		return;
	}

	osg::ref_ptr<osgEarth::Util::MapNode> mapNode = osgEarth::Util::MapNode::findMapNode(node);

<<<<<<< HEAD
    osg::ref_ptr<osg::Group> root = new osg::Group();
    osgEarth::Util::SkyOptions sOptions;
    // sOptions.ambient() = 0.03;
    osgEarth::Util::SkyNode* pSkyNode = osgEarth::Util::SkyNode::create(sOptions, mapNode);

    osgEarth::DateTime dateTime(2021, 4, 27, 8);
    osgEarth::Util::Ephemeris* ephemeris = new osgEarth::Util::Ephemeris;
    pSkyNode->setName("sky");
    pSkyNode->setDateTime(dateTime);
    pSkyNode->setEphemeris(ephemeris);
    pSkyNode->setLighting(true);
    pSkyNode->attach(_viewer,0);

    pSkyNode->addChild(mapNode);
    root->addChild(pSkyNode);

=======
>>>>>>> 5367b4b299b5887bbf7168f96b2438d89b3028dc
	if(!mapNode.valid()) {
		OSG_ALWAYS << "Loaded scene graph does not contain a MapNode - aborting" << std::endl;

		return;
	}

<<<<<<< HEAD
	_viewer->setSceneData(root.get());
=======
	_viewer->setSceneData(mapNode.get());
>>>>>>> 5367b4b299b5887bbf7168f96b2438d89b3028dc

    _viewer->realize();

    _initialized = true;

}

//Draw
void OsgMainApp::draw()
{
    _viewer->frame();

    _frameTouchBeganEvents = NULL;
    _frameTouchMovedEvents = NULL;
    _frameTouchEndedEvents = NULL;
}

//Events
// Events originate in the Java code
// Attach handlers to each type of event
static bool flipy = true;
void OsgMainApp::touchBeganEvent(int touchid,float x,float y)
{
    if (!_frameTouchBeganEvents.valid()) {
        if(_viewer){
            _frameTouchBeganEvents = _viewer->getEventQueue()->touchBegan(touchid, osgGA::GUIEventAdapter::TOUCH_BEGAN, x, flipy ? _bufferHeight-y : y);
        }
    } else {
        _frameTouchBeganEvents->addTouchPoint(touchid, osgGA::GUIEventAdapter::TOUCH_BEGAN, x, flipy ? _bufferHeight-y : y);
    }
}

void OsgMainApp::touchMovedEvent(int touchid,float x,float y)
{
    if (!_frameTouchMovedEvents.valid())
    {
        if(_viewer){
            _frameTouchMovedEvents = _viewer->getEventQueue()->touchMoved(touchid, osgGA::GUIEventAdapter::TOUCH_MOVED, x, flipy ? _bufferHeight-y : y);
        }
    } else {
        _frameTouchMovedEvents->addTouchPoint(touchid, osgGA::GUIEventAdapter::TOUCH_MOVED, x, flipy ? _bufferHeight-y : y);
    }
}

void OsgMainApp::touchZoomEvent(double delta)
{
	osgEarth::Util::EarthManipulator* manip = dynamic_cast<osgEarth::Util::EarthManipulator*>(_viewer->getCameraManipulator());
	manip->zoom(0.0, delta);
}

void OsgMainApp::touchEndedEvent(int touchid,float x,float y,int tapcount)
{
    if (!_frameTouchEndedEvents.valid())
    {
        if(_viewer){
            _frameTouchEndedEvents = _viewer->getEventQueue()->touchEnded(touchid, osgGA::GUIEventAdapter::TOUCH_ENDED, x, flipy ? _bufferHeight-y : y,tapcount);
        }
    } else {
        _frameTouchEndedEvents->addTouchPoint(touchid, osgGA::GUIEventAdapter::TOUCH_ENDED, x, flipy ? _bufferHeight-y : y,tapcount);
    }
}

void OsgMainApp::clearEventQueue()
{
    //clear our groups
    _frameTouchBeganEvents = NULL;
    _frameTouchMovedEvents = NULL;
    _frameTouchEndedEvents = NULL;

    //clear the viewers queue
    _viewer->getEventQueue()->clear();
}
