#include <string.h>
#include <jni.h>
#include <android/log.h>

#include <iostream>

#include "OsgMainApp.hpp"

OsgMainApp mainApp;

extern "C" {
    JNIEXPORT void JNICALL Java_com_example_oedemo_osgNativeLib_init(JNIEnv * env, jobject obj, jint width, jint height);
    JNIEXPORT void JNICALL Java_com_example_oedemo_osgNativeLib_step(JNIEnv * env, jobject obj);
    JNIEXPORT void JNICALL Java_com_example_oedemo_osgNativeLib_touchBeganEvent(JNIEnv * env, jobject obj, jint touchid, jfloat x, jfloat y);
    JNIEXPORT void JNICALL Java_com_example_oedemo_osgNativeLib_touchMovedEvent(JNIEnv * env, jobject obj, jint touchid, jfloat x, jfloat y);
    JNIEXPORT void JNICALL Java_com_example_oedemo_osgNativeLib_touchZoomEvent(JNIEnv * env, jobject obj, jdouble delta);
    JNIEXPORT void JNICALL Java_com_example_oedemo_osgNativeLib_touchEndedEvent(JNIEnv * env, jobject obj, jint touchid, jfloat x, jfloat y, jint tapcount);
    JNIEXPORT void JNICALL Java_com_example_oedemo_osgNativeLib_clearEventQueue(JNIEnv * env, jobject obj);
    JNIEXPORT void JNICALL Java_com_example_oedemo_osgNativeLib_loadEarth(JNIEnv * env, jobject obj, jstring path);
};

JNIEXPORT void JNICALL Java_com_example_oedemo_osgNativeLib_init(JNIEnv * env, jobject obj, jint width, jint height){
    mainApp.initOsgWindow(0,0,width,height);
}
JNIEXPORT void JNICALL Java_com_example_oedemo_osgNativeLib_step(JNIEnv * env, jobject obj){
    mainApp.draw();
}
JNIEXPORT void JNICALL Java_com_example_oedemo_osgNativeLib_touchBeganEvent(JNIEnv * env, jobject obj, jint touchid, jfloat x, jfloat y){
    mainApp.touchBeganEvent(touchid,x,y);
}
JNIEXPORT void JNICALL Java_com_example_oedemo_osgNativeLib_touchMovedEvent(JNIEnv * env, jobject obj, jint touchid, jfloat x, jfloat y){
    mainApp.touchMovedEvent(touchid,x,y);
}
JNIEXPORT void JNICALL Java_com_example_oedemo_osgNativeLib_touchZoomEvent(JNIEnv * env, jobject obj, jdouble delta){
    mainApp.touchZoomEvent(delta);
}
JNIEXPORT void JNICALL Java_com_example_oedemo_osgNativeLib_touchEndedEvent(JNIEnv * env, jobject obj, jint touchid, jfloat x, jfloat y, jint tapcount){
    mainApp.touchEndedEvent(touchid,x,y,tapcount);
}
JNIEXPORT void JNICALL Java_com_example_oedemo_osgNativeLib_clearEventQueue(JNIEnv * env, jobject obj)
{
    mainApp.clearEventQueue();
}

JNIEXPORT void JNICALL Java_com_example_oedemo_osgNativeLib_loadEarth(JNIEnv * env, jobject obj, jstring path){
    const char *filepath = env->GetStringUTFChars(path, JNI_FALSE);
    mainApp.setEarthFile(filepath);
}

